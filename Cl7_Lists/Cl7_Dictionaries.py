import collections

def main():
    empty_dictionary = {} #Empty dictionary
    man_dictionary = {'name': 'Bob', 'age': 40} #Two-item dictionary
    data_with_man_dictionary = {'cto': {'name': 'Bob', 'age': 40}} #Nesting
    
    man2_dictionary = dict(name='Bob', age=40)
    man3_dictionary = dict([('name', 'Bob'), ('age', 40)])
    man4_dictionary = dict(zip(['name', 'age'], ['Bob', 40]))
    man5_dictionary = dict.fromkeys(['name', 'age'], 'AnyValue')
    
    keys = ['spam', 'eggs', 'toast']
    vals = [1, 3, 5]
    from_zip_dictionary = dict(zip(keys, vals))
    
    dict_for_sorting = {'k1':'v', 'k3':'v', 'k2':'va', 'k4':'vb', 'k6':'va', 'k5':'va'}
    print("Not sorted keys:")
    for k in dict_for_sorting:
        print(k, end=' ')

    print("\nSorted keys:")
    sorted_keys = sorted(dict_for_sorting.items())
    for k in sorted_keys:
        print(k, end=' ')
    print()

    ordreder_dict = collections.OrderedDict(sorted(dict_for_sorting.items()))
    ordreder_dict['k0'] = 'va'
    print("Keys of sorted dict:")
    for k in ordreder_dict:
        print(k, end=' ')
    print()

    #Alternative construction techniques:
    #keywords, key / value pairs, zipped key / value pairs, key lists
    man_dictionary['name'] = 'Jack'
    data_with_man_dictionary['cto']['age'] = 40
    #Indexing by key
    if 'age' in man5_dictionary: #Membership: key present test
        print("'age' in man5_dictionary")
    print("D.keys():", man5_dictionary.keys())
    print("D.values():", man5_dictionary.values())
    print("D.items():", man5_dictionary.items())
    #D.copy()
    #D.clear()
    #D.update(D2)
    #D.get(key, -1)
    #D.pop(key, -2)
    #D.setdefault(key, -3)
    #D.popitem()
    #len(D)# Length: number of stored entries
    #D[key] = 42 #Adding/changing keys
    #del D[key] #Deleting entries by key
    #list(D.keys()) #Dictionary views (Python 3.X)
    #D1.keys() & D2.keys() #Dictionary views (Python 3.X)
    
    #D.viewkeys(), D.viewvalues() #Dictionary views (Python 2.7)
    #D = {x: x * 2 for x in range(10)} #Dictionary comprehensions(Python 3.X, 2.7)

    c = collections.Counter(['spam', 'egg', 'spam', 'counter', 'counter', 'counter'])
    for (key, value) in c.items():
        print(key, value)

    c = collections.Counter(a=4, b=2, c=0, d=-2)
    d = collections.Counter(a=1, b=2, c=3, d=4)
    c.subtract(d)
    vrs = vars(c)
    print("The end")

if (__name__ == "__main__"):
    main()


