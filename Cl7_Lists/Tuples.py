import collections


def main():
    t1 = () #An empty tuple
    t2 = (0,) #A one-item tuple (not an expression)
    t3 = (0, 'Ni', 1.2, 3) #A four-item tuple
    t4 = 0, 'Ni', 1.2, 3 #Another four-item tuple (same as prior line)

    t5 = ('Bob', ('dev', 'mgr')) #Nested tuples
    t6 = tuple('spam') #Tuple of items in an iterable
    print(t3[0])
    print(t5[0][1])
    print(t4[1:3])
    print(len(t4))
    print(t3+t5)
    print(t3*3)

    for x in t5:
       print(x)
    print(t4.index('Ni'))
    print(t4.count('Ni'))
    emp_named_tuple = collections.namedtuple('Emp', ['name', 'jobs']) #Named tuple extension type
    emp = emp_named_tuple(name='Bob', jobs=['j1', 'j2'])
    emp_as_dict = emp._asdict()
    print(emp.name, emp.jobs)
    tpl = {'a':1, 'b':2}, {'c':1, 'd':2}
    a = {}
    a[tpl] = 10
    print(type(tpl))

    val = tpl[0]['a']
    tpl[0]['a'] = 10
    a1, *a2, a3 = (1, 2, 3, 4, 5, 6)

    print()

if (__name__ == "__main__"):
    main()

