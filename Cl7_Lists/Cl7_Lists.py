def custom_sort(a):
    if isinstance(a, str):
        return 0
    return a
def main():
    #Creation

    L = [] #An empty list
    L = [123, 'abc', 1.23, {}] #Four items: indexes 0..3
    L = ['Bob', 40.0, ['dev', 'mgr']] #Nested sublists
    L = list('spam')
    L = list(range(-4, 4))

    #operations

    l10 = [1, 2, 3]
    l11 = [1, 2, 3]
    
    l2 = [4, 5, 6]
    l10 = l10 + l2 #l10 recreation
    l11 += l2
    
    L = [1, 2, 3] + [4, 5, 6] + ["90"] #concatenation
    L2 = L*4
    L[0] = 111 # no changes to L2
    
    L2 = [L]*4
    L[0] = 0 # no changes to L2
    

    L2 = L + L + L
    L[0] = 222 

    res = [c*2 for c in 'Hello']
    res[2:4] = [100]
    res.append(24)
    res.sort(key=custom_sort)
    del(res[1:])

    L3 = [1, 2, 3, 4, 5, 6]
    L3[1:] = []
    L3[0] = []
    L3 = [1]
    L3[0:1] = []
    
    print("The end")
if (__name__ == "__main__"):
    main()

