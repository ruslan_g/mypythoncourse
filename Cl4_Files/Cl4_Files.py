import struct
import binascii

def read_and_write_files():
    f = open('data.txt', 'w')
    f.write('Hello hello\n')
    f.write('world\n')
    f.close()

    f = open('data.txt') # 'r' (read) is the default processing mode
    text = f.read() # Read entire file into a string
    print(text)

    for line in open('data.txt'): print(line)

def binary_read_write_files():
    # > - big endian
    # i4sh - int, 4 char, short
    dbl = 123.456
    #packed = struct.pack('@i4shd', 7, b'spam', 8, dbl)
    packed = struct.pack('@d', dbl)
    print(binascii.hexlify(packed))


def main():
    read_and_write_files()
    binary_read_write_files()

if (__name__ == "__main__"):
    main()
