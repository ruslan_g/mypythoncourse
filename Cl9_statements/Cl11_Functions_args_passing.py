def func_not_changing(a):
   a = 99

def func_changing(a):
   a += [99]

def return_multiple(x, y):
    return x, y # Return multiple new values in a tuple

def func_with_3_args(a, b, c):
    print(a, b, c)

def func_uses_defaults(a, b = 10, c = 30):
    print(a, b, c, end='')

def f_with_args(*args):
    for arg in args:
        print(arg)

def f_with_args_and_1_posit(a, *args):
    print(f"type of args:{type(args)}")

    print(f"Positional argument 'a':{a}")
    for arg in args:
        print(arg)

def f_with_kwargs(**args):
    print(f"type of args:{type(args)}")
    for (k,v) in args.items():
        print(f"{k}:{v}")

def f_with_kwargs_and_1_posit(a, **args):
    print(f"type of args:{type(args)}")
    print(f"Positional argument 'a':{a}")
    for (k,v) in args.items():
        print(f"{k}:{v}")

def func_with_list(a, b, c):
    print(a, b, c)

def func_with_list_and_default(a, b, c, d = 10):
    print(a, b, c, d)

def kwonly(a, *, b, c):
    print(a, b, c)

def main():
    func_with_list(*[1, 2, 3])
    func_with_list(*{'a':11, 'b':12, 'c':13})
    func_with_list(**{'a':11, 'b':12, 'c':13})
    #func_with_list(**{'a':11, 'b':12, 'c':13, 'd':13})
    func_with_list_and_default(**{'a':11, 'b':12, 'c':13})
    f_with_args(1, 2, "a_string")
    f_with_args_and_1_posit(1, 2, 3, 4, 5, 6)
    f_with_kwargs(xCoors=1, yCoord=2, desc="a_string")
    f_with_kwargs_and_1_posit(a = 9,xCoors=1, yCoord=2, desc="a_string")
    b = 12
    func_not_changing(b)
    print(b)

    lst = [1, 2, 3]
    func_changing(lst)
    print(lst)

    L = [1, 2]
    func_changing(lst[:]) # Pass a copy, so our 'L' does not change

    a, b = return_multiple(1, 2)
    func_uses_defaults(1)
    func_uses_defaults(1, 2)
    func_uses_defaults(1, 2, 3)
    func_with_3_args(c = 10, b = 20, a = 30)
    func_with_3_args(1, c = 2, b = 3)

    print()

if __name__ == "__main__":
    main()

