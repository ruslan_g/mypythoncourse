def func_not_returning():
    print("I'm a function that doesn't return anything")

def func_returning_val():
    return 12345

def func_receiving_returning_val(v):
    return 2 * v

def func_mul(a, b):
    return a * b

def using_nested_func():
    def f1():
        X = 88
        def f2():
            print(X) # Remembers X in enclosing def scope
        return f2 # Return f2 but don't call it
    action = f1() # Make, return function
    action() # Call it now: prints 88

def maker_funct_with_state(N):
    def action(X): # Make and return action
        return X ** N # action retains N from enclosing scope
    return action

def maker_funct_with_state_list(lst = [4, 5, 6, 7]):
    def action(X): # Make and return action
        return lst * X # action retains N from enclosing scope
    return action

def makeActions_wrong():
    acts = []
    for i in range(5): # Tries to remember each i
        acts.append(lambda x: i ** x) # But all remember same last i!
    return acts

def makeActions_right():
    acts = []
    for i in range(5): # Use defaults instead
        acts.append(lambda x, i=i: i ** x) # Remember current i
    return acts

global_Z = 1
x = "x_x"
def play_with_global_Z():
    #global global_Z, if it's needed to change a global variable
    print(global_Z) # trying to print local global_Z, defined (beacause assigned) below
    global_Z = 100

def play_with_nonlocal_func():
    x = "John"
    def myfunc2():
        nonlocal x
        x = "hello"
    def myfunc3():
        #global x
        x = "world"
    myfunc3() 
    return x

def enclosing_scope_defaults():
    x = 88
    def f2(x=x+1): # Remember enclosing scope X with defaults
        print(x)
    f2()
    
def main():
    enclosing_scope_defaults.someattr = 12345
    enclosing_scope_defaults()
    print(play_with_nonlocal_func())
    global x
    print(x)
    maker = maker_funct_with_state_list([1, 2, 3, 4])
    res = maker(2)
    res = maker(3)
    maker_default = maker_funct_with_state_list()
    res = maker_default(2)


    func_not_returning()
    x = func_returning_val()
    print(x)
    x = func_receiving_returning_val(20)
    print(x)

    print(func_mul(10, 11))

    play_with_global_Z()
    print("global_Z = ", global_Z)

if __name__ == "__main__":
    main()

