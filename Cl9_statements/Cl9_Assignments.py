def main():
    # Assignments
    val1 = 1 # Basic assignment
    val2 = 2
    tupA, tupB = val1, val2 # Tuple assignment. A, B # Like A = nudge; B = wink
    [lstC, lstD] = [val1, val2] # List assignment
    (tupA2, tupB2) = [val1, val2] # List assignment
    [lstC2, lstD2] = (val1, val2) # List assignment
    # error [lstC3, lstD3] = (val1,) # List assignment
    ((a, b), c) = ('SP', 'AM')
    #extended seq
    seq = [1, 2, 3, 4]
    a, *b = seq
    *a, b = seq
    a, b, c, *d = seq #is always assigned a list:
    a, b, c, d, *e = seq # empty list
    #a, *b, c, *d = seq
    print("The end")

if __name__ == "__main__":
    main()

