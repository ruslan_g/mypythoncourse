def main():
    x = 10
    y = 20
    if x > y:
        x = 1
        y = 2
    else:
        x = y = 0
        
    a = 1; b = 2; print(a + b)
    c = 0
    if (a == 1 and
        b == 2 and
        c == 3):
        print('spam' * 3)

    if a == 1 and \
        b == 2 and \
        c == 3:
        print('spam' * 3)
            
    if x > y: print(x)

    while True:
        reply = input('Enter text:')
        if reply == 'stop': break
        print(reply.upper())

    reply = input('Enter text:')
    if reply == 'stop':
        return
    elif not reply.isdigit():
        print('Bad!' * 8)
    else:
        num = int(reply)
        if num < 20:
            print('low')
        else:
            print(num ** 2)

    A = Y if X else Z

    x = 'spam'
    while x: # While x is not empty
        print(x, end=' ') # In 2.X use print x,
        break
        continue
        x = x[1:]
    else:
        pass
    for x in ["spam", "eggs", "ham"]:
        print(x, end=' ')

    for (a, b, c) in [(1, 2, 3), (4, 5, 6)]: # Used in for loop
        print(a, b, c)

    items = ["aaa", 111, (4, 5), 2.01] # A set of objects
    tests = [(4, 5), 3.14] # Keys to search for

    for key in tests: # For all keys
        for item in items: # For all items
            if item == key: # Check for match
                print(key, "was found")
            break
        else:
            print(key, "not found!")

    for i in range(3):
        print(i, 'Pythons')

    S = 'spam'
    for (offset, item) in enumerate(S):
        print(item, 'appears at offset', offset)
if __name__ == "__main__":
    main()
