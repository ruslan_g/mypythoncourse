import sys
import math

def func1_general():
    print("=" * 30, "General", "=" * 30)
    print(sys.platform)
    print(2 ** 100)
    x = 'Spam!'
    print(x * 8)

    #userInput = input()
    #print("You typed: " + userInput)

    res = 2 ** 1000
    print("Length of 2 ** 1000 = " + str(len(str(res))))
    print("Pi = " + str(math.pi))

    s = "HelloWorld"
    print(s[0], s[1:-1], s[-1])
    print(s[:3], s[5:])
    #s[0] = 'h'
    s = s + "!" + s[3:6]
    print(s)
    lst = list(s)
    print(lst)
    btAr = bytearray(b'hello')
    print(btAr[0],btAr[1],btAr[2],btAr[3],btAr[4])
    # string find, replace, split
    formattedString = "%s,%s" % ("H", "w")
    formattedString2 = "{0},{1}".format("H2", "w2")
    formattedString3 = "{},{}".format("H3", "w3")
    print(formattedString)
    print(formattedString2)
    print(formattedString3)

    encodedString1 = 'A\0B\0C'
    encodedString2 = 'A\nB\nC'
    encodedString3 = 'A\x00B\x00C'

    strQuo = "a'a"
    strQuo = 'a"a'
    strQuo = "''"
    unicodeString = u'sp\xc4m'
    print(unicodeString)
    #pattern matching
def func2_lists():
    print("=" * 30, "Lists", "=" * 30)
    l = [1, 2, 3] #mutable
    l[0] = 0
    l.append("qwerty")
    l.append([10, 20, 30])
    l.extend("asdfg")
    l.extend([100, 200, 300])
    print(l)
    l[4][1] = 11111
    print(l)
    #append, pop, reverse, sort
def func_comprehension():
    M = [[1, 2, 3, 4, 5, 6], [10, 20, 30, 40, 50], [100, 200, 300]]
    col2 = [row[1] for row in M]
    summation = (sum(row) for row in M)
    summationList = [sum(row) for row in M]
    summationList2 = list(map(sum, M))
    {sum(row) for row in M}
    {i : sum(M[i]) for i in range(3)}

def func_dictionaries():
    d = {"company":"Toyota", "model":"Camry", "year":2018 }
    d['year'] -=1
    d['year'] = 2019

    d2 = dict(company="Toyota", model="Camry", year="2018")
    d3 = dict(zip(['company', 'model', 'year'], ["Toyota", "Camry","2018"]))
    d4 = {"company":"Toyota", "model":"Camry", "date":{"year":2018, "month":3} }
    test = 'company' in d4

    keyList = list(d4)
    keyList.sort()
    print(keyList)

def func_tuples():
    t = (10, 20, 30, 40, 10)
    i = t.index(10)
    #t[0] = 1
    t = (2,) + t[1:]

def func_files():
    f = open('data.txt', 'w') # Make a new file in output mode ('w' is write)
    f.write('Hello\n') # Write strings of characters to it
    f.write('world\n') # Return number of items written in Python 3.X
    f.close()

    f = open('data.txt') # 'r' (read) is the default processing mode
    text = f.read()
    for line in open('data.txt'): print(line)
    file = open('data.bin', 'wb')
    file = open('unidata.txt', 'w', encoding='utf-8')

def func_sets():
    X = set('spam') # Make a set out of a sequence in 2.X and 3.X
    Y = {'h', 'a', 'm'} # Make a set with set literals in 3.X and 2.7
    Z = X, Y # A tuple of two sets without parentheses
    A = ({'m', 'a', 'p', 's'}, {'m', 'a', 'h'})
    intersec = X & Y # Intersection
    un = X | Y # Union
    sub = X - Y
    isSuper = X > Y # Superset
    compr = {n ** 2 for n in [1, 2, 3, 4]} # Set comprehensions in 3.X and 2.7

def main():
    func1_general()
    func2_lists()
    func_comprehension()
    func_dictionaries()
    func_tuples()
    func_files()

if (__name__ == "__main__"):
    main()
