import sys

def stringAndQuotes():
    str0  = "Hello"
    str1 = "Hello ' world"
    str2 = 'Hello " world'
    str3 = """
    Hello
    world
    """
    str4 = '''Hello
    world
    !
    '''
    str5 = 'hel''lo'

def stringFormatting():
    str1 = 'That is %d %s bird!' % (1, 'dead')
    str1 = '%s -- %s -- %s' % (42, 3.14159, [1, 2, 3])

    x = 1.23456789
    str1 = '%-6.2f | %05.2f | %+06.1f' % (x, x, x)
    str1 = '%(qty)d more %(food)s' % {'qty': 1, 'food': 'spam'}

    values = {'qty': 10, 'food': 'not a spam'}
    str1 = '%(qty)d more %(food)s' % values
    
    values = (11, 'not a spam!!!')
    str1 = '%d more %s' % values
    
    template = '{0}, {1} and {2}'
    str1 = template.format('spam', 'ham', 'eggs')
    template = '{motto}, {pork} and {food}'
    str1 = template.format(motto='spam', pork='ham', food='eggs')
    template = '{motto}, {0} and {food}' #btw {1} does not exists
    str1 = template.format('ham', motto='spam', food='eggs')
    template = '{}, {} and {}'
    str1 = template.format('spam', 'ham', 'eggs')
    str1 = 'My {1[kind]} runs {0.platform}'.format(sys, {'kind': 'laptop'})
    str1 = 'My {map[kind]} runs {sys.platform}'.format(sys=sys, map={'kind': 'laptop'})
    str1 = '{0:10} = {1:10}'.format('spam', 123.4567)
    str1 = '{0:b}'.format((2 ** 16) - 1)
    str1 = '{:f}, {:.2f}, {:06.2f}'.format(3.14159, 3.14159, 3.14159)

def main():
    stringAndQuotes()
    stringFormatting()

if (__name__ == "__main__"):
    main()
