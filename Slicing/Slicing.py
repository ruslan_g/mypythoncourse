def main():
    str = "abcdefgh"
    print(str[0])
    print(str[-2])
    print(str[1:3])
    print(str[slice(1, 3)])
    print(str[1:])
    print(str[1:-1])
    print(str[-4:-1])
    print(str[::2])
    print(str[::-1])
    print(str[3:1:-1])
    print(str[slice(3,1,-1)])



if (__name__ == "__main__"):
    main()

