def main():
    x = set('abcd')
    x = set(['a', 'b', 'c', 'd'])
    y = set('ecbf')
    print(x|y)
    print(x-y)
    print(x&y)
    print(x^y)
    print("x is superset of y:", x > y)
    print("e is in y:", 'e' in y)
    print("e is in x:", 'e' in x)

    func_methods_of_set()
    #methods: intersetion, add, update, remove

def func_methods_of_set():
    print('-'*30, func_methods_of_set, '-'*30)
    x = set([1, 2, 3, 4, 5])
    y = {4, 5, 6, 7}
    z = x.union(y)
    print("x: ", x)
    print("z = x.union(y), z:", z)

    x = set([1, 2, 3, 4, 5])
    y = {4, 5, 6, 7}
    x.update(y)
    print("x.update(y), x: ", x)

    x = set([1, 2, 3, 4, 5])
    y = {4, 5, 6, 7}
    z = x.intersection(y)
    print("x: ", x)
    print("z = x.intersection(y), z: ", z)

    x = set([1, 2, 3, 4, 5])
    y = {4, 5, 6, 7}
    x.intersection_update(y)
    print("x.intersection_update(y), x: ", x)


    x.add(125)
    print("x: ", x)

    #x.add([1, 2, 3, 4]) # not allowed, list is unhashable
    x.add((1, 2, 3, 4)) # OK, tuple is hashable type
    x.add(frozenset([5, 6, 7, 8]))
    print("x: ", x)

    someDict = {'a':1, 'c':5, 'b':3}

    print("----------- Keys ----------------")
    for key in someDict:
        print(key)

    print("----------- Sorted Keys ----------------")
    for key in sorted(someDict):
        print(key)

    print("----------- Key/Value pairs ----------------")
    for key, value in someDict.items():
        print(key, ':', value)

    setCompr = {ch for ch in "Hello"}
    print(setCompr)


if (__name__ == "__main__"):
    main()
